﻿// Задание 1
{
    Console.WriteLine("Задание 1:");
    int x = 10, y = 12, z = 3;
    x += y - x++ * z;
    Console.WriteLine(x);
    z = --x - y * 5;
    Console.WriteLine(z);
    y /= x + 5 % z;
    Console.WriteLine(y);
    z = x++ + y * 5;
    Console.WriteLine(z);
    x = y - x++ * z;
    Console.WriteLine(x);
}

//Задание 2
Console.WriteLine();
Console.WriteLine("Задание 2:");
{
    {
        //Точное значение среднего
        decimal x = 10, y = 12, z = 3;
        decimal sred = 0;
        sred = (x + y + z) / 3;
        Console.WriteLine("Точное значение среднего (верный вариант):  {0}", sred);
    }
    {
        //Округление из-за целочисленной переменной

        int x = 10, y = 12, z = 3;
        int sred = 0;
        sred = (x + y + z) / 3;
        Console.WriteLine("Округление из-за целочисленной переменной (не верный вариант):  {0}", sred);
    }
}

//Задание 3
Console.WriteLine();
Console.WriteLine("Задание 3:");
{
    double pi = 3.1415;
    double r = 2.25;
    double result = pi * Math.Pow(r, 2);
    Console.WriteLine("Площадь круга = {0}", result);
}

//Задание 4
Console.WriteLine();
Console.WriteLine("Задание 4:");
{
    double pi = 3.1415;
    double r = 3;
    double h = 5;

    double s = 2 * pi * r * (r + h);
    Console.WriteLine("Площадь поверхности цилиндра = {0}", s);
    double v = pi * Math.Pow(r, 2) * h;
    Console.WriteLine("Объем цилиндра = {0}", v);
}

//Задание 5
Console.WriteLine();
Console.WriteLine("Задание 5:");
{ 
    //int uberflu? = 1;
    int _Identifier = 2;
    int \u006fIdentifier = 3;
    //int &myVar = 4;
    int myVariab1le = 5;
    Console.WriteLine("Переменные возможно создать только со следующими именами: _Identifier, \u006fIdentifier, myVariab1le");
}
Console.ReadKey();