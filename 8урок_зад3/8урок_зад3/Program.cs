﻿//Задание 3

class Program
{
    static int Factorial(int n)
    { 
        if(n== 0)
            return 1;
        else
            return n * Factorial(n-1);
    }

    static void Main()
    { 
        int factorial = Factorial(3);
        Console.WriteLine("Возможных маршрутов доставки товаров: {0}", factorial);

        Console.ReadKey();
    }

}