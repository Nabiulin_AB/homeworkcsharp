﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _8урок_зад4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //кнопка вычислить
        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton4.Checked) //деление
            {
                double but4;
                double x;
                double y;
                x = Convert.ToDouble(textBox1.Text);
                y = Convert.ToDouble(textBox2.Text);

                if (y == 0)
                {
                    MessageBox.Show("На ноль делить нельзя!");
                    return;
                }

                but4 = x / y;

                textBox3.Text = but4.ToString();
            }

            if (radioButton3.Checked) //конкатенация
            {
                string but3;
                string x = textBox1.Text;
                string y = textBox2.Text;

                but3 = string.Concat(x, y);
                textBox3.Text = but3.ToString();
            }

            if (radioButton2.Checked) //возведение в степень
            {
                double but2;
                double x;
                double y;
                x = Convert.ToDouble(textBox1.Text);
                y = Convert.ToDouble(textBox2.Text);
                
                but2 = Math.Pow(x, y);

                textBox3.Text = but2.ToString();
            }

            if (radioButton1.Checked) //остаток от деления
            {
                double but1;
                double x;
                double y;
                
                x = Convert.ToDouble(textBox1.Text);
                y = Convert.ToDouble(textBox2.Text);

                if (y == 0)
                {
                    MessageBox.Show("На ноль делить нельзя!");
                    return;
                }

                but1 = x % y;
                
                textBox3.Text = but1.ToString();
            }
        }
        //Значение 1
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }
        //Значение 2
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        //Результат
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        //Деление
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        //Конкатенация
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }
        //Возведение в степень
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }
        //Остаток от деления
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
