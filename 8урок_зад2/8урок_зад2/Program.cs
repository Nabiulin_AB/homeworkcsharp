﻿//Задание 2

class Program
{
    static int kredit = 700;

    static void Ostat(int plat)
    { 
        plat =kredit - plat;
        if (plat < 0)
        {
            Console.WriteLine("Переплата по кредиту составляет: {0}", plat);
            Console.WriteLine("Долг отсутствуе!");
        }
        else
        {
            Console.WriteLine("Сумма задолженности: {0}", plat);
        }
    }

    static void Main()
    {
        Console.WriteLine("У клиента кредин на сумму: {0}", kredit);
        
        Console.WriteLine("Введите сумму платежа: ");
        int plat = Convert.ToInt32(Console.ReadLine());

        Ostat(plat);
               
        Console.ReadKey();
    }

}