﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Задание 2:");

decimal pi = 3.141592653m;
decimal e = 2.7182818284590452m;

Console.WriteLine(pi);
Console.WriteLine(e);
Console.WriteLine();

Console.WriteLine("Задание 3:");

string str1 = "\nмоя строка 1";
string str2 = "\tмоя строка 2";
string str3 = "\aмоя строка 3";

Console.WriteLine(str1);
Console.WriteLine(str2);
Console.WriteLine(str3);


Console.ReadKey();
