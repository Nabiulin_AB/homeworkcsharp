﻿// Задание 2

Console.WriteLine("Задание 2:");
Console.WriteLine();
Console.WriteLine("Первый вариант решения:");
Console.WriteLine();

Console.WriteLine("Введите любое число для проверки на четность числа");

string num = Console.ReadLine();

int n = Convert.ToInt32(num);

if (n % 2 == 1)
{
    Console.WriteLine("Число {0} не четное!", n);
}
else
{
    Console.WriteLine("Число {0} четное!", n);
}
Console.WriteLine();
Console.WriteLine("Второй вариант решения:");
Console.WriteLine();

Console.WriteLine("Введите любое число для проверки на четность числа");

string num1 = Console.ReadLine();

int res;

byte n3 = Convert.ToByte(num1);
byte n2 = 1;

res = n3 & n2;

if (res == 0)
{
    Console.WriteLine("Четное!");
}
else
{
    Console.WriteLine("Не четное!");
}

// Задание 3

Console.WriteLine("Задание 3:");
Console.WriteLine();

int x = 5, y = 10, z = 15;

x += y >> x++ * z;
Console.WriteLine(x);
z = ++x & y * 5;
Console.WriteLine(z);
y /= x + 5 | z;
Console.WriteLine(y);
z = x++ & y * 5;
Console.WriteLine(z);
x= y << x++ ^ z;
Console.WriteLine(x);

// Задание 4

Console.WriteLine("Задание 4:");
Console.WriteLine();

Console.WriteLine("Рачет премии исходя из выслуги лет");
Console.WriteLine("Сколько лет Вы отработали?");

string let = Console.ReadLine();
int let1 = Convert.ToInt32(let);

if (let1 < 5)
{
    Console.WriteLine("Премия составляет 10% от зарплаты");
}
else if (5 <= let1 && let1 < 10)
{
    Console.WriteLine("Премия составляет 15% от зарплаты");
}
else if (10 <= let1 && let1 < 15)
{
    Console.WriteLine("Премия составляет 25% от зарплаты");
}
else if (15 <= let1 && let1 < 20)
{
    Console.WriteLine("Премия составляет 35% от зарплаты");
}
else if (20 <= let1 && let1 < 25)
{
    Console.WriteLine("Премия составляет 45% от зарплаты");
}
else if (let1 >= 25)
{
    Console.WriteLine("Премия составляет 50% от зарплаты");
}
Console.ReadKey();