﻿//Задание 2

Console.WriteLine("Задание 2:");

double operand1 = 5;
double operand2 = 3;

Console.WriteLine("Даны два числа {0} и {1}, над ними можно совершить вычетание, сложение, деление, умножение. Введите желаемое действией одним символом: ", operand1, operand2);
string sign = Console.ReadLine();

switch (sign)
{
    case "-":
        Console.WriteLine(operand1 - operand2);
        break;
    case "+":
        Console.WriteLine(operand1 + operand2);
        break;
    case "*":
        Console.WriteLine(operand1 * operand2);
        break;
    case "/":
        if (operand2 == 0)
        {
            Console.WriteLine("Делить на 0 нельзя!");
            break;
        }
        Console.WriteLine(operand1 / operand2);
        break;
           
        
    default:
        Console.WriteLine("Вы ввели не корректный знак!");
        break;
}

//Задание 3
Console.WriteLine();
Console.WriteLine("Задание 3: Тупое решение");

Console.WriteLine("Введите число от 0 до 100");
string number = Console.ReadLine();

switch(number)
{
    case "0":
    case "1":
    case "2":
    case "3":
    case "4":
    case "5":
    case "6":
    case "7":
    case "8":
    case "9":
    case "10":
    case "11":
    case "12":
    case "13":
    case "14":
        Console.WriteLine("Вы попали в промежуток от 0 до 14");
        break ;
    case "15":
    case "16":
    case "17":
    case "18":
    case "19":
    case "20":
    case "21":
    case "22":
    case "23":
    case "24":
    case "25":
    case "26":
    case "27":
    case "28":
    case "29":
    case "30":
    case "31":
    case "32":
    case "33":
    case "34":
    case "35":
        Console.WriteLine("Вы попали в промежуток от 15 до 35");
        break;
    case "36":
    case "37":
    case "38":
    case "39":
    case "40":
    case "41":
    case "42":
    case "43":
    case "44":
    case "45":
    case "46":
    case "47":
    case "48":
    case "49":
    case "50":
        Console.WriteLine("Вы попали в промежуток от 36 до 50");
        break;
    case "51":
    case "52":
    case "53":
    case "54":
    case "55":
    case "56":
    case "57":
    case "58":
    case "59":
    case "60":
    case "61":
    case "62":
    case "63":
    case "64":
    case "65":
    case "66":
    case "67":
    case "68":
    case "69":
    case "70":
    case "71":
    case "72":
    case "73":
    case "74":
    case "75":
    case "76":
    case "77":
    case "78":
    case "79":
    case "80":
    case "81":
    case "82":
    case "83":
    case "84":
    case "85":
    case "86":
    case "87":
    case "88":
    case "89":
    case "90":
    case "91":
    case "92":
    case "93":
    case "94":
    case "95":
    case "96":
    case "97":
    case "98":
    case "99":
    case "100":
        Console.WriteLine("Вы попали в промежуток от 51 до 100");
        break;

    default:
        Console.WriteLine("Вы ввели число больше 100!");
        break;
}

//Задание 3
Console.WriteLine();
Console.WriteLine("Задание 3: Нормальное решение");


Console.WriteLine("Введите число от 0 до 100");
string number1 = Console.ReadLine();

int n5 = Convert.ToInt32(number1);

int n1 = 14;
int n2 = 35;
int n3 = 50;
int n4 = 100;

if (n5 <= n1)
{
    Console.WriteLine("Вы попали в промежуток от 0 до 14");
}
else if (n5 <= n2 && n5 >= n1)
{
    Console.WriteLine("Вы попали в промежуток от 15 до 35");
}
else if (n5 <= n3 && n5 >= n2)
{
    Console.WriteLine("Вы попали в промежуток от 36 до 50");
}
else if (n5 <= n4 && n5 >= n3)
{
    Console.WriteLine("Вы попали в промежуток от 51 до 100");
}
else
{
    Console.WriteLine("Вы ввели число больше 100!");
}
Console.ReadKey();
  