﻿//Задание 2
Console.WriteLine("Задание 2:");
Console.WriteLine();
// Сумма всех чисел между А и Б
{
    int A = 3, B = 7, C = 0;

    Console.WriteLine("Сумма всех чисел между {0} и {1} пока {0} < {1}", A, B);

    for (int i = A; i < B; i++)
        C = C + i;
    Console.WriteLine("Ответ: {0}", C);
}
// Нечетные значения между А и Б
Console.WriteLine();
{
    int A = 3, B = 7;

    Console.WriteLine("Нечетные числа между {0} и {1} пока {0} < {1}", A, B);

    for (int i = A; i <= B; i++)
    {
        if(i % 2 != 0)
        Console.WriteLine(i);
    }
}
//Задание 3
Console.WriteLine();
Console.WriteLine("Задание 3:");
Console.WriteLine();
{
    Console.WriteLine("Прямоугольник:");
    Console.WriteLine();

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 30; j++)
        {
            Console.Write("*");
        }
        Console.WriteLine();
    }

    Console.WriteLine();
    Console.WriteLine("Прямоугольный треугольник:");
    Console.WriteLine();

    for (int i = 1; i < 15; i++)
    {
        for (int j = 0; j < i; j++)
        {
            Console.Write("*");
        }
        Console.WriteLine();
    }

    Console.WriteLine();
    Console.WriteLine("Равносторонний треугольник:");
    Console.WriteLine();

    for (int i = 0; i < 11; i++)
    {
        Console.WriteLine();
        for (int j = 10, k = 0; j > 0 && k < 10; k++, j--)
        {
            if (i > k || i < j)
            {
                Console.Write(" ");
            }
            else
            {
                Console.Write("*");
            }

        }
    }

    Console.WriteLine();
    Console.WriteLine("Ромб:");
    Console.WriteLine();
    
        int x, y, z = 10;
        int ser = z / 2;
        for (x = 0; x < z; x++)
        {
            for (y = 0; y < z; y++)
            {
                if (x <= ser)
                {
                    if (y >= ser - x && y <= ser + x)
                        Console.Write("*");
                    else
                        Console.Write(" ");
                }
                else
                {
                    if (y >= ser + x - z + 1 && y <= ser - x + z - 1)
                        Console.Write("*");
                    else
                        Console.Write(" ");
                }
            }
            Console.WriteLine();
        }
        Console.ReadKey();
    }
    //Задание 4
    Console.WriteLine();
Console.WriteLine("Задание 4:");
Console.WriteLine();
{
    int counter = 4;      //количество точек доставки
    int factorial = 1;      //сколько раз доставить

    Console.Write("При наличии {0} точек клиента ", counter);

    do
    {
        factorial = factorial * counter;
        counter = counter - 1;
    }
    while (counter > 0);

    Console.WriteLine("возможное количество вариантов доставки = {0}", factorial);
}
Console.ReadKey();