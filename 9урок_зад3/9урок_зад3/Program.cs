﻿// Задание 3
class Program
{
    static int[] MyReverse(int[] mas)
    {
        int a = 0;
        int b = mas.Length;
        int c = b - 1;

        for (int i = 0; i < c; i++)
        {
            a = mas[i];
            mas[i] = mas[c];
            mas[c] = a;
            c--;
        }

        return mas;
    }

    static int[] SubArray(int[] mas, int index, int count)
    {
        int[] mas1 = new int[count];
        int k = 0;

        for (int j = 0; j < mas.Length; j++)
        {
            if (mas.Length > index)
            {
                mas[k] = mas[index];
            }
            else
            {
                mas[k] = 1;
            }
            index++;
            k++;
        }
        return mas1;
    }

    static void Main()
    {
        int[] mas = new int[5];

        for (int i = 0; i < mas.Length; i++)
        {
            mas[i] = i * 2;
        }


        Console.Write("Введите индекс массива:");
        int index = Convert.ToInt32(Console.ReadLine());

        Console.Write("Введите количество елементов:");
        int count = Convert.ToInt32(Console.ReadLine());

        mas = SubArray(mas, index, count);

        for (int i = 0; i < mas.Length; i++)
        {
            Console.Write(mas[i]);
        }

        mas = new int[10];

        // Заполнение массива.
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i] = i * 2;
        }

        mas = MyReverse(mas);

        Console.WriteLine();

        for (int i = 0; i < mas.Length; i++)
        {
            Console.Write(mas[i]);
        }

        Console.ReadKey();
    }
}